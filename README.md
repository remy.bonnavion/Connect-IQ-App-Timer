# ConnectIQ-App-Timer

![0](https://gitlab.com/ravenfeld/Connect-IQ-App-Timer/raw/develop/screenshot/0.png)
![0](https://gitlab.com/ravenfeld/Connect-IQ-App-Timer/raw/develop/screenshot/1.png)
![0](https://gitlab.com/ravenfeld/Connect-IQ-App-Timer/raw/develop/screenshot/2.png)
![0](https://gitlab.com/ravenfeld/Connect-IQ-App-Timer/raw/develop/screenshot/3.png)

# Link

[Connect IQ](https://apps.garmin.com/fr-FR/apps/12a98a6c-4909-41bc-b1aa-f8e2de2c9f59)

[Allprojet](https://apps.garmin.com/fr-FR/developer/9a164185-3030-48d9-9aef-f5351abe70d8/apps)